import java.util.List;
import java.util.*;
import java.util.ArrayList; 
/* 
# 1
 
You are given a collection of ABC blocks (maybe like the ones you had when you were a kid https://xcdn.next.co.uk/Common/Items/Default/Default/ItemImages/AltItemShot/315x472/319399s4.jpg).
There are twenty blocks with two letters on each block. A complete alphabet is guaranteed amongst all sides of the blocks. The sample collection of blocks:
(B O) (X K) (D Q)(C P)(N A)(G T)(R E)(T G) (Q D) (F S) (J W) (H U) (V I) (A N) (O B) (E R) (F S) (L Y) (P C) (Z M)
Task
Write a function that takes a string (word) and determines whether the word can be spelled with the given collection of blocks.

The rules are simple:

  Once a letter on a block is used that block cannot be used again
  The function should be case-insensitive
  Show the output for the following 7 words in the following example

Example results

"AND"     True
"BARK"    True
"BOOK"    False
"TREAT"   True
"COMMON"  False
"SQUAD"   True
"CONFUSE" True

Voy a suponer que no hay caras repetidas
*/

public class Ejercicio1{

    public static boolean possible(String target, List<String> sides){

        String ans[][] = new String[sides.size()][2];

        for(int i=0; i<sides.size(); i++){
            ans[i][0] = sides.get(i);
            ans[i][1] = "False";
        }

        boolean found = true;

        for(int i=0; i<target.length(); i++){
            found = false;
            char c = target.charAt(i);

            for(int j=0; j<sides.size(); j++){
                if(ans[j][0].indexOf(c) != -1 && ans[j][1] != "True"){
                    ans[j][1] = "True";
                    found = true;
                    break;
                }
            }
            if(!found) break;
        }
        return found;
       
    }

    public static void main(String a[]){
        List<String> sides = new ArrayList<>(Arrays.asList
                ("B O", "X K", "D Q", "C P", "N A", "G T", "R E", "T G", "Q D", "F S",
                        "J W", "H U", "V I", "A N", "O B", "E R", "F S" ,"L Y" ,"P C", "Z M"));

        List<String> targets = new ArrayList<>(Arrays.asList(
           "AND", "BARK", "BOOK", "TREAT", "COMMON", "SQUAD", "CONFUSE"
        ));
        // Ejecutando que los tests del enunciado
        for(String target: targets) System.out.println(possible(target,sides));
    }
}