import java.util.List;
import java.util.ArrayList;
/* 
# 2
Pattern searching

Having the following string AABAACAADAABAABA, please find how many times you can find the pattern AABA (the string and the pattern to found could be changed)

Example:
Input: AABAACAADAABAABA
Pattern: AABA
Result: Pattern found at 0, 9 and 12
*/

public class Ejercicio2 {
    public static StringBuilder patternSearching(String s, String pattern){
        StringBuilder message = new StringBuilder();

        List<Integer> ans = new ArrayList<>();

        for(int i=0; i<s.length() - pattern.length() + 1; i++){
            boolean matched = true;
            int indexFound = i;
            int j = 0;
            while(matched && j< pattern.length() && i<s.length()){
                if(s.charAt(i) != pattern.charAt(j)){
                    matched = false;
                }
                i++;
                j++;
            }
            i = indexFound;
            if(matched && j==pattern.length()) {ans.add(indexFound);}
        }

        if(ans.isEmpty()){
            message.append("Pattern was not found");
        }else{
            message.append("Pattern found at ");

            for(int i=0; i<ans.size() -1; i++){
                message.append(ans.get(i) + ", ");
            }
            message.append("and " + ans.get(ans.size()-1));
        }

        return message.deleteCharAt(message.lastIndexOf(","));
    }
    public static void main(String[] a){
        String input = "AABAACAADAABAABA";
        String pattern = "AABA";
        //Test del enunciado del problema
        System.out.println(patternSearching(input, pattern));
    }
    
}
